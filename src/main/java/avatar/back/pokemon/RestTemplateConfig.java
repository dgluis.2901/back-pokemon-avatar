package avatar.back.pokemon;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

	//Esta clase sirve para consumir un servicio de una api externa
	@Bean("ClienteRest")
	public RestTemplate registrarRestTemplate() {
		return new RestTemplate();
	}
}
