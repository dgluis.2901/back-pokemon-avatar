package avatar.back.pokemon.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import avatar.back.models.DetallePokemon;
import avatar.back.models.EvolutionChainRoot;
import avatar.back.models.Pokemon;
import avatar.back.models.PokemonSpecies;
import avatar.back.models.Results;

@RestController
public class PokemonREST {

	static final String URL_BASE_API_POKEMON = "https://pokeapi.co/api/v2/";

	@Autowired
	private RestTemplate clienteRest;

// Metodo para listar los pokemones
	@GetMapping("/pokemones")
	public List<Results> pokemones() {
		ResponseEntity<PokemonSpecies> res = clienteRest.getForEntity(URL_BASE_API_POKEMON + "pokemon-species",
				PokemonSpecies.class);

		if (res.getBody().getResult() != null && !res.getBody().getResult().isEmpty()) {
			return res.getBody().getResult();
		} else
			return null;
	}

	// Metodo para siguiente pagina (limit cantidad de datos , offset rango de pag)
	@GetMapping("/siguiente")
	public List<Results> pokemonesPagSiguiente(@RequestParam Integer limit, @RequestParam Integer offset) {
		ResponseEntity<PokemonSpecies> res = clienteRest.getForEntity(
				URL_BASE_API_POKEMON + "pokemon-species?" + "limit=" + limit + "&offset=" + offset,
				PokemonSpecies.class);
		return res.getBody().getResult();

	}

	// Metodo para siguiente pagina (limit cantidad de datos , offset rango de pag)
	@GetMapping("/anterior")
	public List<Results> pokemonesPagAnterior(@RequestParam Integer limit, @RequestParam Integer offset) {
		ResponseEntity<PokemonSpecies> res = clienteRest.getForEntity(
				URL_BASE_API_POKEMON + "pokemon-species?" + "limit=" + limit + "&offset=" + offset,
				PokemonSpecies.class);
		return res.getBody().getResult();

	}

// Metodo para ver el detalle de un pokemon en especifico (id : es el id del pokemon)
	@GetMapping("/detalle/pokemon")
	public DetallePokemon detallePokemon(@RequestParam Integer id) {
		ResponseEntity<DetallePokemon> res = clienteRest
				.getForEntity(URL_BASE_API_POKEMON + "pokemon-species/" + id + "/", DetallePokemon.class);
		return res.getBody();

	}
	
// Metodo para ver el detalle de un pokemon en especifico - habilidades (id : es el id del pokemon)
		@GetMapping("/detalle/pokemonv2")
		public Pokemon detallePokemonv2(@RequestParam Integer id) {
			ResponseEntity<Pokemon> res = clienteRest
					.getForEntity(URL_BASE_API_POKEMON + "pokemon/" + id + "/", Pokemon.class);
			return res.getBody();

		}

	// Metodo para ver la cadena de evolucion un pokemon en especifico (id : es el id del
	// pokemon)
	@GetMapping("/evolution-chain/pokemon")
	public EvolutionChainRoot EvolutionChain(@RequestParam Integer id) {
		ResponseEntity<EvolutionChainRoot> res = clienteRest
				.getForEntity(URL_BASE_API_POKEMON + "evolution-chain/" + id + "/", EvolutionChainRoot.class);
		return res.getBody();

	}

}
