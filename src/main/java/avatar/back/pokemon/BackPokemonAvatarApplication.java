package avatar.back.pokemon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackPokemonAvatarApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackPokemonAvatarApplication.class, args);
	}

}
