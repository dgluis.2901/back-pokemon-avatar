package avatar.back.models;

import java.util.ArrayList;

public class EvolvesTo {

    public ArrayList<EvolutionDetail> evolution_details;
    public ArrayList<Object> evolves_to;
    public boolean is_baby;
    public Species species;
	public ArrayList<EvolutionDetail> getEvolution_details() {
		return evolution_details;
	}
	public void setEvolution_details(ArrayList<EvolutionDetail> evolution_details) {
		this.evolution_details = evolution_details;
	}
	public ArrayList<Object> getEvolves_to() {
		return evolves_to;
	}
	public void setEvolves_to(ArrayList<Object> evolves_to) {
		this.evolves_to = evolves_to;
	}
	public boolean isIs_baby() {
		return is_baby;
	}
	public void setIs_baby(boolean is_baby) {
		this.is_baby = is_baby;
	}
	public Species getSpecies() {
		return species;
	}
	public void setSpecies(Species species) {
		this.species = species;
	}
    
    
}
