package avatar.back.models;

import java.util.ArrayList;

public class Pokemon {

	 public ArrayList<Ability> abilities;
	    public int base_experience;
	    public int height;
	    public int id;
	    public boolean is_default;
	    public String location_area_encounters;
	    public String name;
	    public int order;
	    public int weight;
		public ArrayList<Ability> getAbilities() {
			return abilities;
		}
		public void setAbilities(ArrayList<Ability> abilities) {
			this.abilities = abilities;
		}
		public int getBase_experience() {
			return base_experience;
		}
		public void setBase_experience(int base_experience) {
			this.base_experience = base_experience;
		}
		public int getHeight() {
			return height;
		}
		public void setHeight(int height) {
			this.height = height;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public boolean isIs_default() {
			return is_default;
		}
		public void setIs_default(boolean is_default) {
			this.is_default = is_default;
		}
		public String getLocation_area_encounters() {
			return location_area_encounters;
		}
		public void setLocation_area_encounters(String location_area_encounters) {
			this.location_area_encounters = location_area_encounters;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getOrder() {
			return order;
		}
		public void setOrder(int order) {
			this.order = order;
		}
		public int getWeight() {
			return weight;
		}
		public void setWeight(int weight) {
			this.weight = weight;
		}
	    
	    
}
