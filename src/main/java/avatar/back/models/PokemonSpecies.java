package avatar.back.models;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonSpecies implements Serializable {

	private Integer count;
	private String next;
	private String previus;
	@JsonUnwrapped
	private List<Results> results;
	
	
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public String getNext() {
		return next;
	}
	public void setNext(String next) {
		this.next = next;
	}
	public String getPrevius() {
		return previus;
	}
	public void setPrevius(String previus) {
		this.previus = previus;
	}
	public List<Results> getResult() {
		return results;
	}
	public void setResult(List<Results> results) {
		this.results = results;
	}
	@Override
	public String toString() {
		return "PokemonSpecies [count=" + count + ", next=" + next + ", previus=" + previus + ", results=" + results
				+ "]";
	}
	
	
}
