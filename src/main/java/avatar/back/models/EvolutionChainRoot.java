package avatar.back.models;

public class EvolutionChainRoot {

	    public Object baby_trigger_item;
	    public Chain chain;
	    public int id;
		public Object getBaby_trigger_item() {
			return baby_trigger_item;
		}
		public void setBaby_trigger_item(Object baby_trigger_item) {
			this.baby_trigger_item = baby_trigger_item;
		}
		public Chain getChain() {
			return chain;
		}
		public void setChain(Chain chain) {
			this.chain = chain;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
	
	    
}
