package avatar.back.models;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class DetallePokemon {

	private Integer base_happiness;
	private Integer capture_rate;
	private String name;
	private Evolution_chain evolution_chain;
	
	public Integer getBase_happiness() {
		return base_happiness;
	}
	public void setBase_happiness(Integer base_happiness) {
		this.base_happiness = base_happiness;
	}
	public Integer getCapture_rate() {
		return capture_rate;
	}
	public void setCapture_rate(Integer capture_rate) {
		this.capture_rate = capture_rate;
	}

	public Evolution_chain getEvolution_chain() {
		return evolution_chain;
	}
	public void setEvolution_chain(Evolution_chain evolution_chain) {
		this.evolution_chain = evolution_chain;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "DetallePokemon [base_happiness=" + base_happiness + ", capture_rate=" + capture_rate
				+ ", evolution_chain=" + evolution_chain + "]";
	}


	
	
	
}
